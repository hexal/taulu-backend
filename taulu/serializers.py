from rest_framework import serializers
from taulu.models import Project

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['pk', 'title', 'description']
