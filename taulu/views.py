from taulu.models import Project
from rest_framework import viewsets
from rest_framework import permissions
from taulu.serializers import ProjectSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
