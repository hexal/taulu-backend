import pytest
from rest_framework.test import APIClient
from taulu.models import Project


@pytest.fixture
def api_client():
    return APIClient()

@pytest.fixture
def project():
    project = Project.objects.create(
            title='Testiprojekti',
            description='Tämä on testikuvaus',
    )
    return project
