import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_project_exists(api_client, project):
    
    response = api_client.get(reverse('projects-list'))
    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]['pk'] == project.pk
