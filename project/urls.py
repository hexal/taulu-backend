from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from taulu import views

router = routers.DefaultRouter()
router.register(r'projects', views.ProjectViewSet, 'projects')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('projects/', include('rest_framework.urls')),
]